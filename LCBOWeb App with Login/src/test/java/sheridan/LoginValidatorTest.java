package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

		
	@Test
	public void testIsValidLoginRegularLength( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Dipe123" ) );
	}
	
	@Test
	public void testIsValidLoginExpectionLength( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Dipe  123" ) );
	}

	@Test
	public void testIsValidLoginBoundaryInLength( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Dip123" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutLength( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Dip" ) );
	}
	
		
	@Test
	public void testIsValidLoginRegularCharacter( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Dip123" ) );
	}
	
	@Test
	public void testIsValidLoginExpectionCharacter( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Dip12@" ) );
	}
	@Test
	public void testIsValidLoginBoundaryInCharacter( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Dip123" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutCharacter( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Dipe1" ) );
	}

	
}
