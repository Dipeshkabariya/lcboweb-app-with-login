package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		String regex="^[A-Za-z0-9]*$";
		if (loginName.length( ) >= 6 && loginName.matches(regex)){
			return true;
		} else {
			return false;
		}
	}
}
